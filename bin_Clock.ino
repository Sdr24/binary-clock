const int latchPin_S = 12; // Pin connected to ST_CP of 74HC595
const int clockPin_S = 13; // Pin connected to SH_CP of 74HC595
const int dataPin_S = 11; // Pin connected to DS of 74HC595


const int latchPin_M = 9; // Pin connected to ST_CP of 74HC595
const int clockPin_M = 10; // Pin connected to SH_CP of 74HC595
const int dataPin_M = 8; // Pin connected to DS of 74HC595

const int hourPins[] = { 7, 6, 5, 4 }; //we aren't using the shift register for our hours

const int pmPin = 2; //pin that controls the pm indicator LED

bool pmBit;
int hour;
int minute_L, minute_S;
//  10s place  1s place
int second_L, second_S;



void setup() {

 //TIME SET hopefully self explanatory
hour = 5;
minute_L = 5;
minute_S = 5;
second_L = 5;
second_S = 0;
pmBit = true;

// set pins to output
pinMode(latchPin_S, OUTPUT);
pinMode(clockPin_S, OUTPUT);
pinMode(dataPin_S, OUTPUT);

pinMode(latchPin_M, OUTPUT);
pinMode(clockPin_M, OUTPUT);
pinMode(dataPin_M, OUTPUT);

for(int i = 0; i < 4; i++) {
  pinMode(hourPins[i], OUTPUT);
}

pinMode(pmPin, OUTPUT);
}

void writeSecond() {
  byte second = second_L; //start with the 10s place digit
  second <<= 4; //shift it over to the left-most 3 LEDs by pushing it off the right-most 4
  second += second_S; //then just add the 1s place digits without any fancy bit shifting

  //now what is REALLY nice is that we can just write these bytes straight to the shift register 
  //since our wiring is good, our life is easy

  // Output low level to latchPin
  digitalWrite(latchPin_S, LOW);
  // Send serial data to 74HC595
  shiftOut(dataPin_S, clockPin_S, LSBFIRST, second);
  // Output high level to latchPin, and 74HC595 will update the data to the paralleloutput port.
  digitalWrite(latchPin_S, HIGH);

  
}

void writeMinute() {
  //ditto as 
  byte minute = minute_L;
  minute <<= 4;
  minute += minute_S;
  
  // Output low level to latchPin
  digitalWrite(latchPin_M, LOW);
  // Send serial data to 74HC595
  shiftOut(dataPin_M, clockPin_M, LSBFIRST, minute);
  // Output high level to latchPin, and 74HC595 will update the data to the paralleloutput port.
  digitalWrite(latchPin_M, HIGH);

  
}

void writeHour() {
  //we dont have any fancy shift register on our hour display
  //so we have to convert to binary manually
  
  int temp = hour;

  //start on the right-most LED
  for(int i = 0; i < 4; i++) {
    //with that LED, print 0 or 1, depending on binary
    digitalWrite(hourPins[i], temp % 2);
    temp /= 2; //repeatedly divide by 2, keeping the remainder (remainder is printed above)
  }
}

void writePM() {
  digitalWrite(pmPin, pmBit); //ez
}



void loop() {
  writePM();
  for(; hour <= 12; hour++) {
    
    if(hour == 12) {
      pmBit = !pmBit;
      writePM();
    }
    
    writeHour();
    for(; minute_L < 6; minute_L++) {
      writeMinute();
      for(; minute_S < 10; minute_S++) {
         writeMinute();
        for(; second_L < 6; second_L++) {
          writeSecond();
          for(; second_S < 10; second_S++) {
            writeSecond();
            delay(999); 
            delayMicroseconds(181); //calibrated to be as close to 1 second as possible
          }
          second_S = 0;
        }
        second_L = 0;
      }
      minute_S = 0;
    }
    minute_L = 0;
  }
  hour = 1;
  
}
