# binary clock

Super simple binary clock using an arduino, two shift registers, and a bunch of LEDs

![clock](https://gitlab.com/Sdr24/binary-clock/-/raw/master/clock.jpg)

## hardware look

![overhead](https://gitlab.com/Sdr24/binary-clock/-/raw/master/overhead.jpg)

The red led is the PM bit: on during the afternoon, off in the morning. The yellow LEDs are the hour bits, green is minutes and blue is seconds. Minutes and seconds have two sets, the first three LEDs are the first digit and the last four LEDs are the second digit. Since hours and minutes only go up to 60 exclusive, we do not need more than three bits on the first minute/second digit.  
   
The minute/second bits are connect into a 74HC595 shift register. More specifically, the 74HC595's Q1 output is connected the slowest varrying binary digit, while the Q7 output is connected to the fastest varrying binary digit (aka the farthest right LEDs).
The hour bits are connected directly into arduino, ditto with the pm bit.

## software
Since the hour bits are directly connected to the arduino, we must figure out in software which bits should be on and which should be off. Therefore, we end up converting the hour integer into binary.   
   
The minute/second bits are connect into the 74HC595 shift register in such a way that we can simply serial write the binary number we want to display. This saves the trouble of logically converting into binary, like we had to do for the hour bits.  
  
We are using arduino functions to maintain timing, which has its issues. So instead of pausing 1000 ms every second, we instead pause 999 ms + a few micro seconds to keep accuracy.
